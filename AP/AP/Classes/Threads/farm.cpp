#include "farm.h"

namespace AP
{
	Farm::Farm()
	{
	}

	Farm::~Farm()
	{
	}

	void Farm::AddTask(Task* task)
	{
		task_queue_mutex_.lock();

		task_queue_.push(task);

		task_queue_mutex_.unlock();
	}

	void Farm::Run()
	{
		// for the number of CPU's
		for (int i = 0; i < MAX_CPU; i++)
		{
			// start a new thread
			worker_threads_.push_back(new thread(mem_fun(&Farm::WorkerFunction), this));
		}

		for (int i = 0; i < MAX_CPU; i++)
		{
			// join all threads
			worker_threads_[i]->join();
			delete worker_threads_[i];
			worker_threads_[i] = NULL;
		}

		worker_threads_.clear();
	}

	void Farm::WorkerFunction()
	{
		while (!task_queue_.empty())
		{
			task_queue_mutex_.lock();
			Task* t = task_queue_.front();
			task_queue_.pop();
			task_queue_mutex_.unlock();

			t->Run();

			task_queue_mutex_.lock();
			delete t;
			t = NULL;
			task_queue_mutex_.unlock();
		}
	}

	void Farm::CleanUp()
	{
		if (!worker_threads_.empty())
		{
			for (int i = 0; i < MAX_CPU; i++)
			{
				// join all threads
				if (worker_threads_[i] != NULL)
				{
					worker_threads_[i]->join();
					delete worker_threads_[i];
					worker_threads_[i] = NULL;
				}
			}
			worker_threads_.clear();
		}
	}
}