#ifndef _PATHFINDING_TASK_ASTAR_H
#define _PATHFINDING_TASK_ASTAR_H

#include "model\agent.h"
#include "task.h"

namespace AP
{
	class PathfindingTaskAStar : public Task
	{
	public:
		PathfindingTaskAStar(Agent* agent);
		~PathfindingTaskAStar();

		virtual void Run();

	private:
		Agent* agent_;
	};
}

#endif