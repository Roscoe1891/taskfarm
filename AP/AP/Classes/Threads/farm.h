#ifndef _FARM_H
#define _FARM_H

#include <queue>
#include <mutex>
#include <vector>
#include <thread>
#include <functional>
#include <condition_variable>

#include "task.h"

namespace AP
{
	using std::queue;
	using std::mutex;
	using std::vector;
	using std::thread;
	using std::condition_variable;
	using std::mem_fun;

	const int MAX_CPU = 10;

	class Farm
	{
	public:
		Farm();
		~Farm();
		void AddTask(Task* task);
		void Run();
		void CleanUp();

	private:
		void WorkerFunction();

		vector<thread*> worker_threads_; 
		queue<Task*> task_queue_;
		mutex task_queue_mutex_;
	};
}

#endif