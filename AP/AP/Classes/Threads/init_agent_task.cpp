#include "init_agent_task.h"

namespace AP
{
	InitAgentTask::InitAgentTask(Agent* agent, AgentInitData init_data, AgentView* agent_view)
	{
		agent_ = agent;
		init_data_ = init_data;
		agent_view_ = agent_view;
	}

	InitAgentTask::~InitAgentTask()
	{
	}

	void InitAgentTask::Run()
	{
		agent_->Init(init_data_);
		agent_view_->Init(Shape::SPHERE, 8, agent_);
	}
}