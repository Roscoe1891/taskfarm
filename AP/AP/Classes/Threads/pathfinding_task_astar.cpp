#include "pathfinding_task_astar.h"

namespace AP
{
	PathfindingTaskAStar::PathfindingTaskAStar(Agent* agent)
	{
		agent_ = agent;
	}

	PathfindingTaskAStar::~PathfindingTaskAStar()
	{
	}

	void PathfindingTaskAStar::Run()
	{
		// have agent find a path
		agent_->FindPath();
	}
}