#include "pipeline.h"

namespace AP
{
	Pipeline::Pipeline() : agent_(NULL), producer_(NULL), consumer_(NULL)
	{
		agent_ready_ = false;
		running_ = false;
		finished_ = false;
	}

	Pipeline::~Pipeline()
	{

	}

	void Pipeline::Run(Agent* agent)
	{
		running_ = true;
		finished_ = false;
		agent_ready_ = false;
		agent_ = agent;

		// start producer 
		producer_ = new thread(mem_fun(&Pipeline::Producer), this);

		// start consumer
		consumer_ = new thread(mem_fun(&Pipeline::Consumer), this);
	}

	void Pipeline::Update()
	{
		if ((finished_) && (running_))
		{
			EndRun();
		}
	}

	void Pipeline::EndRun()
	{
		producer_->join();
		consumer_->join();

		delete producer_;
		producer_ = NULL;

		delete consumer_;
		consumer_ = NULL;

		running_ = false;
	}

	void Pipeline::Producer()
	{
		unique_lock<mutex> lock(agent_mutex_);

		agent_->ResetNodeMap();

		agent_ready_ = true;
		agent_cv_.notify_one();
	}

	void Pipeline::Consumer()
	{
		unique_lock<mutex> lock(agent_mutex_);
		
		while (!agent_ready_)
		{
			agent_cv_.wait(lock);
		}

		agent_->FindPath();
		agent_->set_waiting(false);

		finished_ = true;
	}

	void Pipeline::CleanUp()
	{
		if ((producer_ != NULL) && (consumer_ != NULL))
		{
			producer_->join();
			consumer_->join();

			delete producer_;
			producer_ = NULL;

			delete consumer_;
			consumer_ = NULL;
		}
	}
}
