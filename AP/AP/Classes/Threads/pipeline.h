#ifndef _PIPELINE_H
#define _PIPELINE_H

#include <condition_variable>
#include <mutex>
#include <thread>
#include <functional>
#include "model\agent.h"

namespace AP
{
	using std::mutex;
	using std::condition_variable;
	using std::unique_lock;
	using std::thread;
	using std::mem_fun;

	class Pipeline
	{
	public:
		Pipeline();
		~Pipeline();

		void Run(Agent* agent);
		void Update();
		void CleanUp();
		void EndRun();

		inline bool finished() { return finished_; }
		inline bool running() { return running_; }

	private:
		void Producer();
		void Consumer();

		thread* producer_;
		thread* consumer_;

		Agent* agent_;
		mutex agent_mutex_;
		condition_variable agent_cv_;
		bool agent_ready_;
		bool finished_;
		bool running_;

	};
}

#endif