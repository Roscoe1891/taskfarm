#ifndef _TASK_H
#define _TASK_H

namespace AP
{
	class Task
	{
	public:
		virtual ~Task();
		virtual void Run() = 0;
	};
}

#endif