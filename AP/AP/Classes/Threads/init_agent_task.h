#ifndef _INIT_AGENT_TASK_H
#define _INIT_AGENT_TASK_H

#include "task.h"
#include "Model\agent.h"
#include "View\agent_view.h"

namespace AP
{
	class InitAgentTask : public Task
	{
	public:
		InitAgentTask(Agent* agent, AgentInitData init_data, AgentView* agent_view);
		~InitAgentTask();

		virtual void Run();

	private:
		Agent* agent_;
		AgentView* agent_view_;
		AgentInitData init_data_;


	};
}

#endif