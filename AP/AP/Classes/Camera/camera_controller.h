#ifndef _CAMERA_CONTROLLER_H
#define _CAMERA_CONTROLLER_H

#include "camera.h"
#include "utilities/input.h"

// Camera Control Base Class

namespace AP
{

	class CameraController
	{
	public:

		CameraController();
		virtual ~CameraController();

		void virtual ControllerUpdate(float& dTime) = 0;	// Controller specific update (defined by inherited class)

		void Init(Camera* cam);								// Initialise
		void Update(float& dTime);							// Update

	protected:
		// Control Functions for use by inherited classes
		void MoveUp(float& dTime);
		void MoveDown(float& dTime);
		void MoveRight(float& dTime);
		void MoveLeft(float& dTime);
		void MoveForward(float& dTime);
		void MoveBack(float& dTime);
		void RotateRight(float& dTime);
		void RotateLeft(float& dTime);
		void RotateUp(float& dTime);
		void RotateDown(float& dTime);
		void Reset();

		Camera* camera_;				// The camera this controller controls

		bool update_vectors_;		// true if cam has been moved or rotated this frame

		Vector3 new_position_;		// camera's new position
		Vector3 new_orientation_;		// camera's new orientation

	};
}

#endif