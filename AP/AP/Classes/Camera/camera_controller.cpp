#include "camera_controller.h"

namespace AP
{
	CameraController::CameraController()
	{
	}

	CameraController::~CameraController()
	{
	}

	void CameraController::Init(Camera* cam)
	{
		camera_ = cam;
	}

	void CameraController::Update(float& dTime)
	{
		update_vectors_ = FALSE;									// no controller updates received

		new_position_ = camera_->position();					// update position 
		new_orientation_ = camera_->orientation();				// update rotation

		ControllerUpdate(dTime);

		// if cam has been moved or rotated
		if (update_vectors_)
		{
			camera_->UpdateVectors();
		}
	}

	void CameraController::MoveUp(float& dTime)
	{
		new_position_.Add(camera_->up(), camera_->move_speed() * dTime);
	}

	void CameraController::MoveDown(float& dTime)
	{
		new_position_.Subtract(camera_->up(), camera_->move_speed() * dTime);
	}

	void CameraController::MoveRight(float& dTime)
	{
		new_position_.Add(camera_->right(), camera_->move_speed() * dTime);
	}

	void CameraController::MoveLeft(float& dTime)
	{
		new_position_.Subtract(camera_->right(), camera_->move_speed() * dTime);
	}

	void CameraController::MoveForward(float& dTime)
	{
		new_position_.Add(camera_->forward(), camera_->move_speed() * dTime);
	}

	void CameraController::MoveBack(float& dTime)
	{
		new_position_.Subtract(camera_->forward(), camera_->move_speed() * dTime);
	}

	void CameraController::RotateUp(float& dTime)
	{
		new_orientation_.set_x(camera_->orientation().x() + camera_->rotate_speed() * dTime);
	}

	void CameraController::RotateDown(float& dTime)
	{
		new_orientation_.set_x(camera_->orientation().x() - camera_->rotate_speed() * dTime);
	}

	void CameraController::RotateRight(float& dTime)
	{
		new_orientation_.set_y(camera_->orientation().y() + camera_->rotate_speed() * dTime);
	}

	void CameraController::RotateLeft(float& dTime)
	{
		new_orientation_.set_y(camera_->orientation().y() - camera_->rotate_speed() * dTime);
	}

	void CameraController::Reset()
	{
		camera_->Init(Vector3(0.f, 0.f, 6.f), Vector3(0.f, 0.f, 0.f), Vector3(0.f, 1.f, 0.f));
	}
}



