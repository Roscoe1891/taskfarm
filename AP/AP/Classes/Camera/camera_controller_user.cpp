#include "camera_controller_user.h"

namespace AP
{
	CameraControllerUser::CameraControllerUser()
	{
	}

	CameraControllerUser::~CameraControllerUser()
	{
	}

	void CameraControllerUser::ControllerInit(Camera* cam, Input* in, LPPOINT centre, int move_up, int move_down, int move_left, int move_right, int move_fwd, int move_back, int reset)
	{
		Init(cam);

		input_ = in;
		client_centre_ = centre;

		keyboard_controls_[MOVE_UP] = move_up;
		keyboard_controls_[MOVE_DOWN] = move_down;
		keyboard_controls_[MOVE_LEFT] = move_left;
		keyboard_controls_[MOVE_RIGHT] = move_right;
		keyboard_controls_[MOVE_FORWARD] = move_fwd;
		keyboard_controls_[MOVE_BACK] = move_back;

		keyboard_controls_[RESET] = reset;
	}

	void CameraControllerUser::ControllerUpdate(float& dTime)
	{
		// store mouse movement
		int mouse_move_x = input_->mouse_x() - (int)client_centre_->x;
		int mouse_move_y = input_->mouse_y() - (int)client_centre_->y;

		// if mouse has moved
		if ((mouse_move_x != 0) || (mouse_move_y != 0))
		{
			update_vectors_ = TRUE;	// will need to update vectors

			// increase pitch / yaw by mouse movement
			if ((mouse_move_x) > 0) RotateRight(dTime);
			else if ((mouse_move_x) < 0) RotateLeft(dTime);

			if ((mouse_move_y) > 0) RotateDown(dTime);
			else if ((mouse_move_y) < 0) RotateUp(dTime);

			// set camera to new orientation
			camera_->set_orientation(new_orientation_);
		}

		// if we have received keyboard input
		if (ReceivedKeyInput(input_))
		{
			update_vectors_ = TRUE;	// will need to update vectors

			// update camera position from input

			if (input_->key_down(keyboard_controls_[MOVE_UP])) MoveUp(dTime);
			else if (input_->key_down(keyboard_controls_[MOVE_DOWN])) MoveDown(dTime);

			if (input_->key_down(keyboard_controls_[MOVE_LEFT])) MoveLeft(dTime);
			else if (input_->key_down(keyboard_controls_[MOVE_RIGHT])) MoveRight(dTime);

			if (input_->key_down(keyboard_controls_[MOVE_FORWARD])) MoveForward(dTime);
			else if (input_->key_down(keyboard_controls_[MOVE_BACK])) MoveBack(dTime);

			// set new position
			camera_->set_position(new_position_);

			// reset
			if (input_->key_down(keyboard_controls_[RESET])) Reset();
		}
	}

	bool CameraControllerUser::ReceivedKeyInput(Input* in)
	{
		for (int i = 0; i < MAX_KEYBOARD_CONTROLS; i++)		// check all keyboard controls
		{
			if (in->key_down(keyboard_controls_[i]))		// return true if any are down
				return true;
		}
		return false;
	}


}



