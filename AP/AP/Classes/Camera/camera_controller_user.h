#ifndef _CAMERA_CONTROLLER_USER_H
#define _CAMERA_CONTROLLER_USER_H

#include "camera.h"
#include "utilities/input.h"
#include "camera_controller.h"

// Camera Control User Class

namespace AP
{
#define MAX_KEYBOARD_CONTROLS 7

	class CameraControllerUser : public CameraController
	{
	public:
		enum KeyboardControl { MOVE_UP = 0, MOVE_DOWN, MOVE_LEFT, MOVE_RIGHT, MOVE_FORWARD, MOVE_BACK, RESET };

		CameraControllerUser();
		virtual ~CameraControllerUser();
		// Controller Specific Update (called from base class Update)
		void virtual ControllerUpdate(float& dTime);		
		// Controller Specific Init (also calls base class Init)
		void ControllerInit(Camera* cam, Input* in, LPPOINT centre, int move_up, int move_down, int move_left, int move_right, int move_fwd, int move_back, int reset);

	private:
		bool ReceivedKeyInput(Input* in);				// Returns true if any recognised keyboard controls have been entered
		LPPOINT client_centre_;							// the centre of the window (client co-ords)
		Input* input_;									// input
		int keyboard_controls_[MAX_KEYBOARD_CONTROLS];	// Recognised keyboard controls

	};
}

#endif