#include "camera.h"

namespace AP
{
	Camera::Camera() :
		move_speed_(10), rotate_speed_(120)
	{
	}

	Camera::~Camera()
	{
	}

	void Camera::Init(Vector3 pos, Vector3 look, Vector3 u)
	{
		position_ = pos;
		look_at_ = look;
		up_ = u;
		UpdateVectors();
		orientation_ = Vector3(0.f, 0.f, 0.f);
	}

	void Camera::UpdateVectors()
	{
		float cos_P, cos_Y, cos_R;			// cos pitch, yaw, roll
		float sin_P, sin_Y, sin_R;			// sin pitch, yaw, roll

		cos_P = cosf(orientation_.x() * (float)PI / 180);	// cos pitch
		cos_Y = cosf(orientation_.y() * (float)PI / 180);	// cos yaw
		cos_R = cosf(orientation_.z() * (float)PI / 180);	// cos roll

		sin_P = sinf(orientation_.x() * (float)PI / 180);	// sin pitch
		sin_Y = sinf(orientation_.y() * (float)PI / 180);	// sin yaw
		sin_R = sinf(orientation_.z() * (float)PI / 180);	// sin roll

		// set forward vector
		forward_.set_x(sin_Y * cos_P);
		forward_.set_y(sin_P);
		forward_.set_z(cos_P * -cos_Y);

		// set look at point
		look_at_ = position_ + forward_;

		// set up vector
		up_.set_x((-cos_Y * sin_R) - (sin_Y * sin_P * cos_R));
		up_.set_y(cos_P * cos_R);
		up_.set_z((-sin_Y * sin_R) - (sin_P * cos_R * -cos_Y));

		// calc side vector (right) if required
		right_.Set(forward_.Cross(up_));
	}
}