#ifndef _CAMERA_H
#define _CAMERA_H

#include <cmath>
#include "maths/math_definitions.h"
#include "maths/vector3.h"

// Camera

namespace AP
{

	class Camera
	{
	public:
		Camera();
		~Camera();

		void Init(Vector3 pos, Vector3 look, Vector3 u);
		void UpdateVectors();

		inline const Vector3 position() { return position_; }
		inline void set_position(Vector3 pos) { position_ = pos; }

		inline const Vector3 orientation() { return orientation_; }
		inline void set_orientation(Vector3 orient) { orientation_ = orient; }

		inline void set_pitch(float orient) { orientation_.set_x(orient); }
		inline void set_yaw(float orient) { orientation_.set_y(orient); }

		inline const Vector3 look_at() { return look_at_; }
		inline const Vector3 up() { return up_; }
		inline const Vector3 right() { return right_; }
		inline const Vector3 forward() { return forward_; }

		inline const float& move_speed() { return move_speed_; }
		inline const float& rotate_speed() { return rotate_speed_; }

	private:
		const float move_speed_;
		const float rotate_speed_;
		Vector3 position_;
		Vector3 orientation_;	// (x = pitch, y = yaw, z = roll)
		Vector3 look_at_;
		Vector3 up_;
		Vector3 forward_;
		Vector3 right_;
	};

}

#endif