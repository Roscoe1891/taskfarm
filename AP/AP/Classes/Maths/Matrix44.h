#ifndef _MATRIX_44_H
#define _MATRIX_44_H

#include <cmath>

namespace AP
{
	class Vector3;
	class Vector4;

	using std::cosf;
	using std::sinf;

	class Matrix44
	{
	public:
		void SetIdentity();
		void SetZero();
	
		void RotationX(float rotationRad);
		void RotationY(float rotationRad);
		void RotationZ(float rotationRad);
		void Scale(const Vector3& scale);
		Vector3 GetScale() const;
		void SetTranslation(const Vector3& translation);
		const Vector3 GetTranslation() const;
		void Transpose(const Matrix44& matrix);

		const Matrix44 operator*(const Matrix44& matrix) const;

		float m[4][4];
	};
}

#endif