#ifndef _VECTOR3_H
#define _VECTOR3_H

#include <math.h>

// Vector 3

namespace AP
{

	class Vector3
	{

	public:
		Vector3(float x = 0, float y = 0, float z = 0);
		Vector3 Copy();

		// param in x, y & z for new value
		void Set(float x, float y, float z);
		// param in new value
		void Set(Vector3 v2);

		// return X value
		inline const float x() const { return this->x_; }
		// param in new X value
		inline void set_x(float x) { this->x_ = x; }

		// return Y value
		inline const float y() const { return this->y_; }
		// param in new Y value
		inline void set_y(float y) { this->y_ = y; }

		// return Z value
		inline const float z() const { return this->z_; }
		// param in new Z value
		inline void set_z(float z) { this->z_ = z; }

		// adds v1 to this (scale default = 1)
		void Add(const Vector3& v1, float scale = 1.0);
		// subtracts v1 from this (scale default = 1)
		void Subtract(const Vector3& v1, float scale = 1.0);
		// param in scale value
		void Scale(float scale);
		// returns dot product of this and v2
		float Dot(const Vector3& v2);
		// return cross product of this and v2
		Vector3 Cross(const Vector3& v2);

		void Normalise();		// normalise this
		float Length();			// returns vector3 length
		float LengthSquared();	// returns vector3 length squared

		// returns true if v2 is equal to this
		bool Equals(const Vector3& v2, float epsilon);
		bool Equals(const Vector3& v2);

		// operator overrides
		Vector3 operator+(const Vector3& v2);
		Vector3 operator-(const Vector3& v2);

		Vector3& operator+=(const Vector3& v2);
		Vector3& operator-=(const Vector3& v2);

	private:
		float x_;
		float y_;
		float z_;
	};

}

#endif