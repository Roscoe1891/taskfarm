#include <maths/vector4.h>
#include <maths/matrix44.h>

namespace AP
{
	Vector4::Vector4(const float x, const float y, const float z, const float w)
	{
		this->x_ = x;
		this->y_ = y;
		this->z_ = z;
		this->w_ = w;
	}


	const Vector4 Vector4::Transform(const class Matrix44& _mat) const
	{
		Vector4 result;

		result.x_ = x_*_mat.m[0][0]+y_*_mat.m[1][0]+z_*_mat.m[2][0]+w_*_mat.m[3][0];
		result.y_ = x_*_mat.m[0][1]+y_*_mat.m[1][1]+z_*_mat.m[2][1]+w_*_mat.m[3][1];
		result.z_ = x_*_mat.m[0][2]+y_*_mat.m[1][2]+z_*_mat.m[2][2]+w_*_mat.m[3][2];
		result.w_ = x_*_mat.m[0][3]+y_*_mat.m[1][3]+z_*_mat.m[2][3]+w_*_mat.m[3][3];

		return result;
	}

}