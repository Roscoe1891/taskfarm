#include "vector3.h"
 
namespace AP
{
	Vector3::Vector3(float x, float y, float z) 
	{
		this->x_ = x;
		this->y_ = y;
		this->z_ = z;
	}

	Vector3 Vector3::Copy() 
	{
		Vector3 copy(
			this->x_,
			this->y_,
			this->z_);
		return copy;
	}

	bool Vector3::Equals(const Vector3& v2, float epsilon) 
	{
		return ((abs(this->x_ - v2.x()) < epsilon) &&
			(abs(this->y_ - v2.y()) < epsilon) &&
			(abs(this->z_ - v2.z()) < epsilon));
	}

	bool Vector3::Equals(const Vector3& v2)
	{
		return Equals(v2, 0.00001f);
	}


	float Vector3::Length() 
	{
		return sqrt(this->LengthSquared());
	}

	float Vector3::LengthSquared()
	{
		return (
			this->x_*this->x_ +
			this->y_*this->y_ +
			this->z_*this->z_
			);
	}

	void Vector3::Normalise() 
	{
		float mag = this->Length();
		if (mag) 
		{
			float multiplier = 1.0f / mag;
			this->x_ *= multiplier;
			this->y_ *= multiplier;
			this->z_ *= multiplier;
		}
	}

	Vector3 Vector3::Cross(const Vector3& v2) 
	{
		Vector3 cross(
			(this->y_ * v2.z() - this->z_ * v2.y()),
			(this->z_ * v2.x() - this->x_ * v2.z()),
			(this->x_ * v2.y() - this->y_ * v2.x())
			);
		return cross;
	}

	void Vector3::Subtract(const Vector3& v1, float scale) 
	{
		this->x_ -= (v1.x()*scale);
		this->y_ -= (v1.y()*scale);
		this->z_ -= (v1.z()*scale);
	}

	void Vector3::Set(float x, float y, float z) 
	{
		this->x_ = x;
		this->y_ = y;
		this->z_ = z;
	}

	void Vector3::Set(Vector3 v2) 
	{
		this->x_ = v2.x();
		this->y_ = v2.y();
		this->z_ = v2.z();
	}

	float Vector3::Dot(const Vector3& v2) 
	{
		return (this->x_*v2.x() +
			this->y_*v2.y() +
			this->z_*v2.z()
			);
	}

	void Vector3::Scale(float scale) 
	{
		this->x_ = this->x_ * scale;
		this->y_ = this->y_ * scale;
		this->z_ = this->z_ * scale;
	}

	void Vector3::Add(const Vector3& v1, float scale) 
	{
		this->x_ += (v1.x()*scale);
		this->y_ += (v1.y()*scale);
		this->z_ += (v1.z()*scale);
	}

	Vector3 Vector3::operator+(const Vector3& v2) 
	{
		return Vector3(this->x_ + v2.x(), this->y_ + v2.y(), this->z_ + v2.z());
	}

	Vector3 Vector3::operator-(const Vector3& v2) 
	{
		return Vector3(this->x_ - v2.x(), this->y_ - v2.y(), this->z_ - v2.z());
	}

	Vector3& Vector3::operator+=(const Vector3& v2) 
	{
		this->x_ += v2.x();
		this->y_ += v2.y();
		this->z_ += v2.z();
		return *this;
	}

	Vector3& Vector3::operator-=(const Vector3& v2) 
	{
		this->x_ -= v2.x();
		this->y_ -= v2.y();
		this->z_ -= v2.z();
		return *this;
	}

}