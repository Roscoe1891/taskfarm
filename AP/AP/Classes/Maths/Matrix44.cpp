#include <maths/matrix44.h>
#include <maths/vector3.h>
#include <maths/vector4.h>
#include <cmath>

namespace AP
{

void Matrix44::SetIdentity()
{
	for(int i=0;i<4;++i)
		for(int j=0;j<4;++j)
			if (i == j)
				m[i][j] = 1.0f;
			else
				m[i][j] = 0.0f;

}

void Matrix44::SetZero()
{
	for(int i=0;i<4;++i)
		for(int j=0;j<4;++j)
			m[i][j] = 0.0f;
}

const Matrix44 Matrix44::operator*(const Matrix44& matrix) const
{
	Matrix44 result;

	for (int i=0; i<4; i++)
		for (int j=0; j<4; j++)
			result.m[i][j] =  m[i][0] * matrix.m[0][j] + m[i][1] * matrix.m[1][j] + m[i][2] * matrix.m[2][j] + m[i][3] * matrix.m[3][j];

	return result; 
}


void Matrix44::RotationX(float rotationRad)
{
	m[0][0] = 1.0f;
	m[0][1] = 0.0f;
	m[0][2] = 0.0f;
	m[0][3] = 0.0f;

	m[1][0] = 0.0f;
	m[1][1] = cosf(rotationRad);
	m[1][2] = sinf(rotationRad);
	m[1][3] = 0.0f;

	m[2][0] = 0.0f;
	m[2][1] = -sinf(rotationRad);
	m[2][2] = cosf(rotationRad);
	m[2][3] = 0.0f;

	m[3][0] = 0.0f;
	m[3][1] = 0.0f;
	m[3][2] = 0.0f;
	m[3][3] = 1.0f;
}

void Matrix44::RotationY(float rotationRad)
{
	m[0][0] = cosf(rotationRad);
	m[0][1] = 0.0f;
	m[0][2] = -sinf(rotationRad);
	m[0][3] = 0.0f;

	m[1][0] = 0.0f;
	m[1][1] = 1.0f;
	m[1][2] = 0.0f;
	m[1][3] = 0.0f;

	m[2][0] = sinf(rotationRad);
	m[2][1] = 0.0f;
	m[2][2] = cosf(rotationRad);
	m[2][3] = 0.0f;

	m[3][0] = 0.0f;
	m[3][1] = 0.0f;
	m[3][2] = 0.0f;
	m[3][3] = 1.0f;
}

void Matrix44::RotationZ(float rotationRad)
{
	m[0][0] = cosf(rotationRad);
	m[0][1] = sinf(rotationRad);
	m[0][2] = 0.0f;
	m[0][3] = 0.0f;

	m[1][0] = -sinf(rotationRad);
	m[1][1] = cosf(rotationRad);
	m[1][2] = 0.0f;
	m[1][3] = 0.0f;

	m[2][0] = 0.0f;
	m[2][1] = 0.0f;
	m[2][2] = 1.0f;
	m[2][3] = 0.0f;

	m[3][0] = 0.0f;
	m[3][1] = 0.0f;
	m[3][2] = 0.0f;
	m[3][3] = 1.0f;
}

void Matrix44::Scale(const Vector3& scale)
{
	SetIdentity();
	m[0][0] = scale.x();
	m[1][1] = scale.y();
	m[2][2] = scale.z();
}

Vector3 Matrix44::GetScale() const
{
	Vector3 scale_x(m[0][0], m[0][1], m[0][2]);
	Vector3 scale_y(m[1][0], m[1][1], m[1][2]);
	Vector3 scale_z(m[2][0], m[2][1], m[2][2]);

	return Vector3(scale_x.Length(), scale_y.Length(), scale_z.Length());
}

void Matrix44::SetTranslation(const Vector3& translation)
{
	m[3][0] = translation.x();
	m[3][1] = translation.y();
	m[3][2] = translation.z();
}


const Vector3 Matrix44::GetTranslation() const
{
	return Vector3(m[3][0], m[3][1], m[3][2]);
}

void Matrix44::Transpose(const Matrix44& matrix)
{
	for(int rowNum=0;rowNum<4;++rowNum)
		for(int columnNum=0;columnNum<4;++columnNum)
			m[rowNum][columnNum] = matrix.m[columnNum][rowNum];
}

}