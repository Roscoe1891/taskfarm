#ifndef _VECTOR4_H
#define _VECTOR4_H

namespace AP
{

class Vector4
{
public:
	Vector4(const float x = 0.0, const float y = 0.0, const float z = 0.0, const float w = 1.0f);

	const Vector4 Transform(const class Matrix44& _mat) const;

	float x_;
	float y_;
	float z_;
	float w_; 
};

}

#endif
