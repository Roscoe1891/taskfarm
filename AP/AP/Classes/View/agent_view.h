#ifndef _AGENT_VIEW_H
#define _AGENT_VIEW_H

#include "geometry\shape.h"
#include "model\agent.h"

namespace AP
{
	class AgentView : public Shape
	{
	public:
		AgentView();
		~AgentView();
		void Init(ShapeType type, int polys, Agent* agent);
		void Update();
		void Render();

	private:
		Agent* agent_;	// agent this view is reponsible for showing
		Vector3 translation_;
		Vector3 k_colour_;
		Vector3 k_scale_;
	};
}

#endif