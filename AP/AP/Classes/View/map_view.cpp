#include "map_view.h"

namespace AP
{
	MapView::MapView()
	{
	}

	MapView::~MapView()
	{
	}

	void MapView::Init(Map* map)
	{
		map_ = map;

		floor_tile_.Init(Shape::PLANE, 1);
		blocked_tile_.Init(Shape::CUBE, 1);
	}

	void MapView::Render()
	{
		for (int y = 0; y < HEIGHT; y++)
		{
			for (int x = 0; x < WIDTH; x++)
			{
				glPushMatrix();
					glTranslatef((GLfloat)x, (GLfloat)y, 0.f);
					if (map_->GetNodeAtPoint(Point(x, y)).blocked())
					{
						glColor3f(0.25f, 0.25f, 0.25f);
						glTranslatef(0.0f, 0.0f, 1.0f);
						blocked_tile_.Render(Shape::ARRAY_ELEMENT);
					}	
					else
					{
						glColor3f(0.5f, 0.5f, 0.5f);
						floor_tile_.Render(Shape::ARRAY_ELEMENT);

						glPushMatrix();
							glColor3f(0.0f, 0.5f, 0.5f);
							glPolygonMode(GL_FRONT, GL_LINE);
							glTranslatef(0.f, 0.f, 0.01f);
							floor_tile_.Render(Shape::ARRAY_ELEMENT);
							glPopMatrix();
						glPolygonMode(GL_FRONT, GL_FILL);
					}
				glPopMatrix();
			}
		}
	}

}