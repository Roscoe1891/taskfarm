#ifndef _MAP_VIEW_H
#define _MAP_VIEW_H

#include "model\map.h"
#include "geometry\shape.h"

namespace AP
{
	class MapView
	{
	public:
		MapView();
		~MapView();
		void Init(Map* map);
		void Render();

	private:
		Map* map_;		// The map this view is reponsible for showing
		Shape floor_tile_;
		Shape blocked_tile_;
	};

	

}


#endif

