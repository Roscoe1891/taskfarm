#include "agent_view.h"

namespace AP
{
	AgentView::AgentView() : 
		agent_(NULL),
		k_colour_(Vector3(0.5f, 0.f, 0.25f)), k_scale_(Vector3(0.25f, 0.25f, 0.25f))
	{
		translation_ = Vector3(0.f, 0.f, 0.f);
	}

	AgentView::~AgentView()
	{

	}

	void AgentView::Init(ShapeType type, int polys, Agent* agent)
	{
		agent_ = agent;
		Shape::Init(type, polys);
		translation_ = agent_->position();
	}

	void AgentView::Update()
	{
		translation_ = agent_->position();
	}

	void AgentView::Render()
	{
		glPushMatrix();
			glColor3f(k_colour_.x(), k_colour_.y(), k_colour_.z());
			glTranslatef(translation_.x(), translation_.y(), translation_.z());
			glScalef(k_scale_.x(), k_scale_.y(), k_scale_.z());
			Shape::Render(Shape::ARRAY_ELEMENT);
		glPopMatrix();

	}
}