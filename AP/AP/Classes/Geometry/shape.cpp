#include "Shape.h"

namespace AP
{
	Shape::Shape()
	{
		init_origin_ = Vector3(0.0, 0.0, 0.0);
		polygons_ = 0;
		GLuint texture_ = NULL;
	}

	Shape::~Shape()
	{
	}

	void Shape::Init(ShapeType type, int polys, GLuint txr, Vector3 origin)
	{
		shape_type_ = type;
		init_origin_ = origin;
		
		polygons_ = polys;
		texture_ = txr;

		indices_.clear();
		vertices_.clear();
		normals_.clear();
		texture_coords_.clear();

		switch (shape_type_)
		{
		case PLANE:
			GeneratePlane();
			break;
		case DISC:
			GenerateDisc();
			break;
		case CUBE:
			GenerateCube();
			break;
		case SPHERE:
			GenerateSphere();
			break;
		case CYLINDER:
			GenerateCylinder();
			break;
		}
	}

	void Shape::Render(RenderMethod renderMethod)
	{
		// Enable arrays
		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_NORMAL_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		if (renderMethod == DRAW_ELEMENT) glEnableClientState(GL_INDEX_ARRAY);

		// Set array pointers
		glVertexPointer(3, GL_FLOAT, 0, vertices_.data());
		glNormalPointer(GL_FLOAT, 0, normals_.data());
		glTexCoordPointer(2, GL_FLOAT, 0, texture_coords_.data());
		if (renderMethod == DRAW_ELEMENT) glIndexPointer(GL_UNSIGNED_BYTE, 0, indices_.data());

		// Set texture
		if (texture_ != NULL)
		{
			glEnable(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D, texture_);
		}
		else
		{
			glDisable(GL_TEXTURE_2D);
		}

		//int vertexCount = vertex_multiplier * polygons * polygons;
		int vertexCount = vertices_.size() / 3;

		switch (renderMethod)
		{
		case ARRAY_ELEMENT:
			glBegin(GL_TRIANGLES);

			for (int i = 0; i < vertexCount; i++)
			{
				glArrayElement(i);
			}

			glEnd();
			break;

		case DRAW_ARRAYS:
			glDrawArrays(GL_TRIANGLES, 0, vertexCount);
			break;

		case DRAW_ELEMENT:
			glDrawElements(GL_TRIANGLES, vertexCount, GL_UNSIGNED_BYTE, indices_.data());
			break;
		}

		// disable arrays
		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_NORMAL_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		if (renderMethod == DRAW_ELEMENT) glDisableClientState(GL_INDEX_ARRAY);
	}

	// Param 'normal' can be set as x, y, or z (the axis which acts as the normal)
	// Param faceup allows for the plane to be 'flipped' (defaults are x: facing right, y: facing up, z: facing forward)
	void Shape::GeneratePlane(char normal, bool face_up, bool cube_component)
	{
		for (int y = 0; y < polygons_; ++y)
		{
			for (int x = 0; x < polygons_; ++x)
			{
				// NORMALS

				// Set normal as vector3
				Vector3 norm;
				switch (normal)
				{
				case 'x':
					norm = Vector3(1.0, 0.0, 0.0);
					break;
				case 'y':
					norm = Vector3(0.0, 1.0, 0.0);
					break;
				case 'z':
					norm = Vector3(0.0, 0.0, 1.0);
					break;
				default:
					// WINDOW!
					break;
				}

				// add components to normals vector 
				for (int i = 0; i < 6; ++i)
				{
					normals_.push_back(norm.x());
					normals_.push_back(norm.y());
					normals_.push_back(norm.z());
				}
				
				// VERTICES
				Matrix44 verts;						// 4x4 Matrix to store the 4 vertices

				float offset = polygons_ * 0.5f;		// offset ensures origin of quad is true origin
		
				// populate matrix based on a 'default' quad
				// (drawn in the xy plane, facing camera)
				verts.m[0][0] = x + 1.0f - offset;
				verts.m[0][1] = y + 1.0f - offset;
				verts.m[0][2] = 1.0f - offset;
				verts.m[0][3] = 1.0f;
			
				verts.m[1][0] = x - offset;
				verts.m[1][1] = y + 1.0f - offset;
				verts.m[1][2] = 1.0f - offset;
				verts.m[1][3] = 1.0f;

				verts.m[2][0] = x - offset;
				verts.m[2][1] = y - offset;
				verts.m[2][2] = 1.0f - offset;
				verts.m[2][3] = 1.0f;

				verts.m[3][0] = x + 1.0f - offset;
				verts.m[3][1] = y - offset;
				verts.m[3][2] = 1.0f - offset;
				verts.m[3][3] = 1.0f;

				// perform rotations if necessary
				// (i.e. if quad is not 'default' quad)
				Matrix44 rotation_matrix;
				rotation_matrix.SetIdentity();

				Vector3 translation = init_origin_;
				Vector3 translation_offset = norm;
				translation_offset.Scale(polygons_ - 1.0f);

				switch (normal)
				{
				case 'x':
					if (face_up)
					{
						rotation_matrix.RotationY((float)PI * 0.5f);
						translation += translation_offset;
					}
					else
					{
						rotation_matrix.RotationY(-(float)PI * 0.5f);
						translation -= translation_offset;
					}
					break;
				case 'y':
					if (face_up)
					{
						rotation_matrix.RotationX(-(float)PI * 0.5f);
						translation += translation_offset;
					}
					else
					{
						rotation_matrix.RotationX((float)PI * 0.5f);
						translation -= translation_offset;
					}
					break;
				case 'z':
					if (face_up)
					{
						if (cube_component) translation += translation_offset;
					}
					else
					{
						rotation_matrix.RotationY((float)PI);
						translation -= translation_offset;
					}
					break;
				}
				
				verts = verts * rotation_matrix;

				// translate vertices to init origin
				Matrix44 translate_matrix;
				translate_matrix.SetIdentity();
				translate_matrix.SetTranslation(translation);
				verts = verts * translate_matrix;

				// add vertices to vertex vector
				for (int v = 0; v < 3; ++v) vertices_.push_back(verts.m[0][v]);

				for (int v = 0; v < 3; ++v) vertices_.push_back(verts.m[1][v]);

				for (int v = 0; v < 3; ++v) vertices_.push_back(verts.m[3][v]);

				for (int v = 0; v < 3; ++v) vertices_.push_back(verts.m[2][v]);

				for (int v = 0; v < 3; ++v) vertices_.push_back(verts.m[3][v]);

				for (int v = 0; v < 3; ++v) vertices_.push_back(verts.m[1][v]);
					

				// TEXTURE COORDS
				float uv_increment = 1.0f / polygons_;
				float u_pos = uv_increment * (float)x;
				float v_pos = uv_increment * (polygons_ - ((float)y + 1.0f));
				
				texture_coords_.push_back(u_pos + uv_increment);
				texture_coords_.push_back(v_pos);

				texture_coords_.push_back(u_pos);
				texture_coords_.push_back(v_pos);

				texture_coords_.push_back(u_pos + uv_increment);
				texture_coords_.push_back(v_pos + uv_increment);

				texture_coords_.push_back(u_pos);
				texture_coords_.push_back(v_pos + uv_increment);

				texture_coords_.push_back(u_pos + uv_increment);
				texture_coords_.push_back(v_pos + uv_increment);

				texture_coords_.push_back(u_pos);
				texture_coords_.push_back(v_pos);
			}
		}
	}

	void Shape::GenerateCube()
	{
		GeneratePlane('x', true, true);
		GeneratePlane('x', false, true);

		GeneratePlane('y', true, true);
		GeneratePlane('y', false, true);

		GeneratePlane('z', true, true);
		GeneratePlane('z', false, true);
	}

	void Shape::GenerateDisc()
	{
		float angle_offset = (2 * (float)PI) / polygons_;

		Vector3 norm;
		norm = Vector3(0.0f, 0.0f, 1.0f);

		double angle;
		float x, y, u, v;

		for (int i = 0; i < polygons_; ++i)
		{
			for (int iv = 0; iv < 3; ++iv) { vertices_.push_back(0.0f); }
			for (int it = 0; it < 2; ++it) { texture_coords_.push_back(0.5f); }
			
			angle = (angle_offset * i);
			x = (float)cos(angle);
			y = (float)sin(angle);
			u = (-x / 2) + 0.5f;
			v = (y / 2) + 0.5f;

			vertices_.push_back(x);
			vertices_.push_back(y);
			vertices_.push_back(0.0f);

			texture_coords_.push_back(u);
			texture_coords_.push_back(v);

			if (i + 1 == polygons_) angle = 0.0f;
			else angle = (angle_offset * (i + 1));
			x = (float)cos(angle);
			y = (float)sin(angle);
			u = (-x / 2) + 0.5f;
			v = (y / 2) + 0.5f;

			vertices_.push_back(x);
			vertices_.push_back(y);
			vertices_.push_back(0.0f);

			texture_coords_.push_back(u);
			texture_coords_.push_back(v);

		}

		for (size_t in = 0; in < vertices_.size(); in +=3)
		{
			normals_.push_back(norm.x());
			normals_.push_back(norm.y());
			normals_.push_back(norm.z());
		}

		for (size_t iv = 0; iv < vertices_.size(); iv += 3)
		{
			vertices_[iv] += init_origin_.x();
			vertices_[iv + 1] += init_origin_.y();
			vertices_[iv + 2] += init_origin_.z();
		}
		
	}

	void Shape::GenerateSphere()
	{
		const float lats_interval = 1.f / polygons_;
		const float longs_interval = 1.f / polygons_;

		const float theta = GetTheta(polygons_);
		const float delta = GetDelta(polygons_);

		for (int longs = 0; longs < polygons_; ++longs)
		{
			for (int lats = 0; lats < polygons_; ++lats)
			{
				// VERTICES
				vertices_.push_back(GetX(lats, longs, theta, delta));
				vertices_.push_back(GetY(longs, delta));
				vertices_.push_back(GetZ(lats, longs, theta, delta));

				vertices_.push_back(GetX(lats + 1, longs + 1, theta, delta));
				vertices_.push_back(GetY(longs + 1, delta));
				vertices_.push_back(GetZ(lats + 1, longs + 1, theta, delta));

				vertices_.push_back(GetX(lats, longs + 1, theta, delta));
				vertices_.push_back(GetY(longs + 1, delta));
				vertices_.push_back(GetZ(lats, longs + 1, theta, delta));

				vertices_.push_back(GetX(lats + 1, longs + 1, theta, delta));
				vertices_.push_back(GetY(longs + 1, delta));
				vertices_.push_back(GetZ(lats + 1, longs + 1, theta, delta));

				vertices_.push_back(GetX(lats, longs, theta, delta));
				vertices_.push_back(GetY(longs, delta));
				vertices_.push_back(GetZ(lats, longs, theta, delta));

				vertices_.push_back(GetX(lats + 1, longs, theta, delta));
				vertices_.push_back(GetY(longs, delta));
				vertices_.push_back(GetZ(lats + 1, longs, theta, delta));

				// TEXTURE COORDS
				texture_coords_.push_back(GetU(lats_interval, lats));
				texture_coords_.push_back(GetV(longs_interval, longs));

				texture_coords_.push_back(GetU(lats_interval, lats + 1));
				texture_coords_.push_back(GetV(longs_interval, longs + 1));

				texture_coords_.push_back(GetU(lats_interval, lats));
				texture_coords_.push_back(GetV(longs_interval, longs + 1));

				texture_coords_.push_back(GetU(lats_interval, lats + 1));
				texture_coords_.push_back(GetV(longs_interval, longs + 1));

				texture_coords_.push_back(GetU(lats_interval, lats));
				texture_coords_.push_back(GetV(longs_interval, longs));
				
				texture_coords_.push_back(GetU(lats_interval, lats + 1));
				texture_coords_.push_back(GetV(longs_interval, longs));
			}
		}

		// NORMALS
		// (normals are the same as vertices here)
		for (size_t n = 0; n < vertices_.size(); ++n)
		{
			normals_.push_back(vertices_[n]);
		}

		// Move Sphere to init position
		for (size_t v = 0; v < vertices_.size(); v += 3)
		{
			vertices_[v] += init_origin_.x();
			vertices_[v + 1] += init_origin_.y();
			vertices_[v + 2] += init_origin_.z();
		}
	}

	void Shape::GenerateCylinder()
	{
		// VERTICES
		const float longSegments = 4;
		const float angle_interval = (float)((2 * PI) / polygons_);

		const float lats_interval = 1.f / polygons_;
		const float longs_interval = 1.f / longSegments;

		for (int longs = 0; longs < longSegments; ++longs)
		{
			for (int lats = 0; lats < polygons_; ++lats)
			{
				double angle;
				float x, z;

				angle = angle_interval * (lats + 1);	// 0
				x = float(-cos(angle));
				z = float(sin(angle));
				vertices_.push_back(x);
				vertices_.push_back((float)(longs + 1));
				vertices_.push_back(z);

				angle = angle_interval * lats;			// 1
				x = float(-cos(angle));
				z = float(sin(angle));
				vertices_.push_back(x);
				vertices_.push_back((float)(longs + 1));
				vertices_.push_back(z);

				angle = angle_interval * lats;			// 3
				x = float(-cos(angle));
				z = float(sin(angle));
				vertices_.push_back(x);
				vertices_.push_back((float)longs);
				vertices_.push_back(z);

				angle = angle_interval * (lats + 1);	// 2
				x = float(-cos(angle));
				z = float(sin(angle));
				vertices_.push_back(x);
				vertices_.push_back((float)longs);
				vertices_.push_back(z);

				angle = angle_interval * (lats + 1);	// 3
				x = float(-cos(angle));
				z = float(sin(angle));
				vertices_.push_back(x);
				vertices_.push_back((float)(longs + 1));
				vertices_.push_back(z);

				angle = angle_interval * lats;			// 0
				x = float(-cos(angle));
				z = float(sin(angle));
				vertices_.push_back(x);
				vertices_.push_back((float)longs);
				vertices_.push_back(z);

				// TEXTURE COORDS

				texture_coords_.push_back(GetU(lats_interval, lats + 1));
				texture_coords_.push_back(GetV(longs_interval, longs + 1));

				texture_coords_.push_back(GetU(lats_interval, lats));
				texture_coords_.push_back(GetV(longs_interval, longs + 1));

				texture_coords_.push_back(GetU(lats_interval, lats));
				texture_coords_.push_back(GetV(longs_interval, longs));

				texture_coords_.push_back(GetU(lats_interval, lats + 1));
				texture_coords_.push_back(GetV(longs_interval, longs));

				texture_coords_.push_back(GetU(lats_interval, lats + 1));
				texture_coords_.push_back(GetV(longs_interval, longs + 1));

				texture_coords_.push_back(GetU(lats_interval, lats));
				texture_coords_.push_back(GetV(longs_interval, longs));
			}
		}

		// NORMALS
		// (normals are the same as vertices here)
		for (size_t in = 0; in < vertices_.size(); ++in)
		{
			normals_.push_back(vertices_[in]);
		}

		// Move Cylinder to init position
		for (size_t iv = 0; iv < vertices_.size(); iv += 3)
		{
			vertices_[iv] += (init_origin_.x());
			vertices_[iv + 1] += init_origin_.y() - (longSegments * 0.5f);
			vertices_[iv + 2] += init_origin_.z();
		}

	}

	float Shape::GetTheta(int latitude)
	{
		float theta = 2 * (float)PI / latitude;
		return theta;
	}

	float Shape::GetDelta(int longitude)
	{
		float delta = (float)PI / longitude;
		return delta;
	}

	float Shape::GetX(int latitude, int longitude, float theta, float delta)
	{
		float x = cos(theta * latitude) * sin(delta * longitude);
		return x;
	}

	float Shape::GetY(int longitude, float delta)
	{
		float y = cos(delta * longitude);
		return y;
	}

	float Shape::GetZ(int latitude, int longitude, float theta, float delta)
	{
		float z = sin(theta * latitude) * sin(delta * longitude);
		return z;
	}

	float Shape::GetU(float lat_spacing, int latitude)
	{
		float u = lat_spacing * latitude;
		return u;
	}

	float Shape::GetV(float long_spacing, int longitude)
	{
		float v = long_spacing * longitude;
		return v;
	}
}

