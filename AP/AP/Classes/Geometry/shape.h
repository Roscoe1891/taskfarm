#ifndef _SHAPE_H
#define _SHAPE_H

#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>
#include <vector>

#include "maths/math_definitions.h"
#include "maths/vector3.h"
#include "maths/vector4.h"
#include "maths/matrix44.h"

using std::vector;

// this class represents a primitive shape
// created procedurally on initialisation

namespace AP
{
	class Shape
	{
	public:
		enum ShapeType { PLANE, DISC, CUBE, SPHERE, CYLINDER };
		enum RenderMethod { ARRAY_ELEMENT, DRAW_ARRAYS, DRAW_ELEMENT };

		Shape();
		~Shape();
		void Init(ShapeType type, int polys, GLuint txr = NULL, Vector3 origin = Vector3(0.0, 0.0, 0.0));
		void Render(RenderMethod renderMethod);

	private:
		// Shape Generation Functions
		void GeneratePlane(char normal = 'z', bool face_up = true, bool cube_component = false);		// params for use by generatecube function only (see definition)
		void GenerateDisc();
		void GenerateCube();
		void GenerateSphere();
		void GenerateCylinder();

		// Sphere Utility Functions
		float GetTheta(int latitude);
		float GetDelta(int longitude);
		float GetX(int latitude, int longitude, float theta, float delta);
		float GetY(int longitude, float delta);
		float GetZ(int latitude, int longitude, float theta, float delta);
		float GetU(float lat_spacing, int latitude);
		float GetV(float long_spacing, int longitude);

		vector<GLubyte> indices_;
		vector<float> vertices_;
		vector<float> normals_;
		vector<float> texture_coords_;
	
		ShapeType shape_type_;
		Vector3 init_origin_;				// primitive's initial origin

		int polygons_;
		GLuint texture_;
	};
}

#endif