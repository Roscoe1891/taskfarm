#ifndef _TIMER_H
#define _TIMER_H

#include <windows.h>

// Timer
// This is used to calculate delta time (time elapsed since last update)

namespace AP
{
	class Timer
	{
	public:

		bool Init();			// Initialise
		void CalcDeltaTime();	// Calculate time since last update

		// return delta time
		inline float delta_time() const { return delta_time_; }

	private:
		INT64 frequency_;				// Frequency
		float ticks_per_second_;		// Number of ticks per second
		INT64 start_time_;				// Start Time counter
		float delta_time_;				// Time since last update
	};
}

#endif