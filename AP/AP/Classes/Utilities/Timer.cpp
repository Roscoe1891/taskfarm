#include "timer.h"

namespace AP
{
	bool Timer::Init()
	{
		// Check to see if this system supports high performance timers
		QueryPerformanceFrequency((LARGE_INTEGER*)&frequency_);
		if (frequency_ == 0)
		{
			return false;
		}

		// Find out how many times the frequency counter ticks every second
		ticks_per_second_ = (float)(frequency_);

		QueryPerformanceCounter((LARGE_INTEGER*)&start_time_);

		return true;
	}

	void Timer::CalcDeltaTime()
	{
		INT64 currentTime;
		float timeDifference;

		// Query the current time
		QueryPerformanceCounter((LARGE_INTEGER*)&currentTime);

		// Calculate the difference in time since the last time we queried for the current time
		timeDifference = (float)(currentTime - start_time_);

		// Calculate the frame time by the time difference over the timer speed resolution
		delta_time_ = timeDifference / ticks_per_second_;

		// Restart the timer
		start_time_ = currentTime;

		return;
	}

}