#ifndef _INPUT_H
#define _INPUT_H

// Input
// Provides access to user input (keyboard and mouse)

#include <Windows.h>
#include "Maths\Vector3.h"

namespace AP
{
#define MAX_KEYS 256

	class Input
	{
		// container for mouse info
		typedef struct Mouse
		{
			int x, y;
			bool left, right;
		};

	public:
		void Update();
		bool KeyPressed(int key);		// return true if key has been pressed
		bool KeyReleased(int key);		// return true if key has been released

		// param in key to be set 'down'
		inline void set_key_down(WPARAM key) { current_keys_[key] = true; }
		// param in key to be set 'up'
		inline void set_key_up(WPARAM key) { current_keys_[key] = false; }
		// return true if key is down
		inline bool key_down(int key) { return current_keys_[key]; }
		// param in new mouse position
		inline void set_mouse_pos(POINT pos) { SetCursorPos(pos.x, pos.y); }
		// return mouse x position
		inline int mouse_x() { return mouse_.x; }
		// param in new mouse x position
		inline void set_mouse_x(int pos) { mouse_.x = pos; }
		// return mouse y position
		inline int mouse_y() { return mouse_.y; }
		// param in new mouse y position
		inline void set_mouse_y(int pos) { mouse_.y = pos; }


	private:
		bool current_keys_[MAX_KEYS];		// store state of all keys (true = down)
		bool old_keys_[MAX_KEYS];			// store state of all keys (true = down)
		Mouse mouse_;						// the mouse
	};

}

#endif