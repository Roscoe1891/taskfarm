#ifndef _BENCHMARK_H
#define _BENCHMARK_H


#include <iostream>
#include <fstream>
#include <stack>
#include <chrono>

namespace AP
{
	using std::ofstream;
	using std::cout;
	using std::chrono::duration_cast;
	using std::chrono::milliseconds;

	typedef std::chrono::steady_clock the_clock;

	class Benchmark
	{
	public:
		Benchmark();
		~Benchmark();

		void StartTiming();
		void StopTiming();
		void RecordResult();
		void OutputResult();

	private:
		the_clock::time_point start_time_;
		the_clock::time_point stop_time_;
		int result_;
	};
}

#endif // !_BENCHMARK_H

