#include "Input.h"

namespace AP
{
	
	void Input::Update()
	{
		for (int i = 0; i < MAX_KEYS; i++)
		{
			old_keys_[i] = current_keys_[i];
		}
	}

	bool Input::KeyPressed(int key)
	{
		if (current_keys_[key] && !old_keys_[key])
			return true;
		else
			return false;
	}

	bool Input::KeyReleased(int key)
	{
		if (!current_keys_[key] && old_keys_[key])
			return true;
		else
			return false;
	}

}

