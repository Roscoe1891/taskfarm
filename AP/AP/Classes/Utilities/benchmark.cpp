#include "benchmark.h"

namespace AP
{
	Benchmark::Benchmark()
	{
		result_ = 0;
	}

	Benchmark::~Benchmark()
	{
	}

	void Benchmark::StartTiming()
	{
		start_time_ = the_clock::now();
	}

	void Benchmark::StopTiming()
	{
		stop_time_ = the_clock::now();
	}

	void Benchmark::RecordResult()
	{
		result_ = (int)duration_cast<milliseconds>(stop_time_ - start_time_).count();
	}

	void Benchmark::OutputResult()
	{
		ofstream result("result.txt");
		if (result.is_open())
		{
			result << "Test took " << result_ << " ms." << "\n";
			result.close();
		}
	}

}