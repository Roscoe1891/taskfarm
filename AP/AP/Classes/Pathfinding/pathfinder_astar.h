#ifndef _PATHFINDER_ASTAR_H
#define _PATHFINDER_ASTAR_H

#include <list>
#include <set>
#include <unordered_set>
#include "pathfinder.h"

namespace AP
{
	using std::list;
	using std::unordered_set;

	// PathfinderAstar is used for pathfinding using the A* algorithm
	class PathfinderAStar : public Pathfinder
	{
	public:
		PathfinderAStar();
		~PathfinderAStar();

		// Find and return path between start and end
		virtual stack<Node*> GetPath(Node& startNode, Node& endNode);

		// Get the heuristic value for supplied node in relation to supplied end node
		int GetHeuristic(Node& node, Node& endNode);
		// Get the distance between a node and its neighbour
		int GetDistanceBetween(Node& node, Node& neighbour);

		// insertion, search & print utilities
		void InsertOrdered(std::list<Node*>* list, Node* item);
		list<Node*>::iterator LinearSearch(list<Node*>* list, Node* item);

	private:

	};
}


#endif