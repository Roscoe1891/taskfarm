#ifndef _PATHFINDER_LEE_H
#define _PATHFINDER_LEE__H

#include "pathfinder.h"

namespace AP
{

	class PathfinderLee : public Pathfinder
	{
	public:
		PathfinderLee();
		~PathfinderLee();

		// Find and return path between start and end
		virtual stack<Node*> GetPath(Node& start, Node& end);

	private:

	};

}

#endif // !_PATHFINDER_LEE
