#ifndef _PATHFINDER_H
#define _PATHFINDER_H

#include <chrono>
#include <thread>
#include <stack>

#include "model\map.h"
#include "model\node.h"

namespace AP
{
	using std::chrono::duration_cast;
	using std::chrono::milliseconds;
	using std::this_thread::sleep_for;
	using std::stack;

	// Pathfinder is the abstract base class for pathfinders.
	class Pathfinder
	{
	public:
		Pathfinder();
		virtual ~Pathfinder();

		void Init(Map* map);
		// Find and return a path between start and end
		virtual stack<Node*> GetPath(Node& start, Node& end) = 0;

	protected:
		Map* map_;		// pointer to a map

	};

}

#endif // !_PATHFINDER_H
