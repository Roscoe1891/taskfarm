#ifndef _MAP_H
#define _MAP_H

#include <stack>
#include <iostream>

#include "node.h"
#include "utilities/rng.h"

namespace AP
{
// height and width of the map
#define HEIGHT 52
#define WIDTH 52
// distance between nodes on the map
#define DISTANCE 10
#define DIAGONAL_DISTANCE 14

	typedef Node NodeMap[HEIGHT][WIDTH];

	using std::cout;
	using std::endl;
	using std::stack;

	// The map class contains the array nodes which represents the world
	// It is responsible for intialising this array

	class Map
	{
	public:
		enum DisplayType { CO_ORD = 0, F_SCORE, BLOCKED };

		Map();
		~Map();

		void Init(RNG* rng, int blockedNodes);

		// Display the map (style of display dependent on supplied type)
		void DisplayMap(DisplayType displayType);
		// Display the provided path between start and end on the map
		void DisplayPathOnMap(stack<Node*> path, Node& start, Node& end);

		// Return node @ provided point
		Node& GetNodeAtPoint(const Point point);
		// Return a random node
		Node& GetRandomNode();

		// Return reference to node map
		inline NodeMap& node_map() { return node_map_; }

	private:
		// internal initialisation functions
		void InitNodes();
		void InitNodeNeighbours();
		void InitBlockedNodes();
		void TidyBlockedNodes();
		void BlockBorder();

		NodeMap node_map_;					// The node map
		
		RNG* rng_;					// random number generator
		int blocked_nodes_;			// the number of blocked nodes on the map

	};
}



#endif