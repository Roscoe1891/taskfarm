#include "agent.h"

namespace AP
{
	Agent::Agent()
	{
		waiting_ = false;
		wait_at_next_node_ = false;
		got_path_ = true;
	}

	Agent::~Agent()
	{
	}

	void Agent::Init(Vector3 position, float move_speed, NodeMap& node_map, Node& goal)
	{
		position_ = position;
		move_speed_ = move_speed;
		CopyNodeMap(node_map);
		SetNewGoal(goal);
		waiting_ = false;
		wait_at_next_node_ = false;
		got_path_ = false;
	}

	void Agent::Init(AgentInitData init_data)
	{
		position_ = Vector3(init_data.start->point().x, init_data.start->point().y, 1.0f);
		move_speed_ = init_data.speed;
		CopyNodeMap(init_data.map->node_map());
		SetNewGoal(*init_data.goal);
		waiting_ = false;
		wait_at_next_node_ = false;
		got_path_ = false;
	}

	void Agent::CopyNodeMap(NodeMap& node_map)
	{
		for (int y = 0; y < HEIGHT; ++y)
		{
			for (int x = 0; x < WIDTH; ++x)
			{
				Node* copy_node = &node_map[y][x];

				node_map_[y][x].InitPoint(x, y);
				node_map_[y][x].set_f(0);
				node_map_[y][x].set_g(0);
				node_map_[y][x].set_h(0);
				node_map_[y][x].set_parent(nullptr);
				node_map_[y][x].set_blocked(copy_node->blocked());
			}
		}

		for (int y = 0; y < HEIGHT; ++y)
		{
			for (int x = 0; x < WIDTH; ++x)
			{
				Node* copy_node = &node_map[y][x];

				for (size_t i = 0; i < copy_node->neighbours()->size(); ++i)
				{
					Node* copy_neighbour = copy_node->GetNeighbour(i);
					Node* neighbour = &node_map_[copy_neighbour->point().y][copy_neighbour->point().x];
					node_map_[y][x].AddNeighbour(neighbour);
				}
			}
		}
	}

	void Agent::Update(float& dt)
	{
		if ((!path_.empty()) && (!waiting_))
		{	
			Vector3 dest = Vector3((float)path_.top()->point().x, (float)path_.top()->point().y, 1.0);

			if (!position_.Equals(dest, 0.075f))		// if agent is not within a very short distance of destination
			{
				MoveTo(dt, dest);						// move agent towards destination
			}
			else if (!position_.Equals(dest))			// if agent is within a very short distance of destination, but not exactly at destination
			{
				position_ = dest;						// that's close enough, set position to destination
			}
			else if (!wait_at_next_node_)				// if agent is exactly at destination and is not to wait there
			{
				path_.pop();							// move on to next desination
				if (path_.empty())						// if path is now empty
					waiting_ = true;
			}
			else										// otherwise agent is to wait at this node
			{
				wait_at_next_node_ = false;
				waiting_ = true;
			}
		}

	}

	void Agent::FindPath()
	{
		// clear path
		while (!path_.empty())
		{
			path_.pop();
		}

		Point start = Point((int)position_.x(), (int)position_.y());

		Node& start_node = node_map_[start.y][start.x];

		path_ = pathfinder_astar_.GetPath(start_node, *goal_);

		got_path_ = true;
	}

	void Agent::SetNewGoal(Node& goal)
	{
		goal_ = &node_map_[goal.point().y][goal.point().x];
		got_path_ = false;
	}

	void Agent::MoveTo(float& dt, Vector3 destination)
	{
		Vector3 dir = destination - position_;
		dir.Normalise();
		dir.Scale(move_speed_ * dt);
		position_ += dir;
	}

	void Agent::ResetNodeMap()
	{
		for (int y = 0; y < HEIGHT; ++y)
		{
			for (int x = 0; x < WIDTH; ++x)
			{
				node_map_[y][x].set_f(0);
				node_map_[y][x].set_g(0);
				node_map_[y][x].set_h(0);
				node_map_[y][x].set_parent(nullptr);
			}
		}
	}

}