#include "agent_manager.h"

namespace AP
{
	AgentManager::AgentManager() : map_(NULL)
	{
		updating_paths_ = false;
	}

	AgentManager::~AgentManager()
	{
		task_farm_.CleanUp();
		pipeline_.CleanUp();
	}

	void AgentManager::Init(Map* map)
	{
		map_ = map;

		for (int i = 0; i < MAX_AGENTS; i++)
		{
			Node& random_start_node = map_->GetRandomNode();
			Node& random_goal_node = map_->GetRandomNode();

			AgentInitData init_data;
			init_data.start = &random_start_node;
			init_data.goal = &random_goal_node;
			init_data.speed = 5.0f;
			init_data.map = map_;

			task_farm_.AddTask(new InitAgentTask(&agents_[i], init_data, &agent_views_[i]));
		}

		task_farm_.Run();

		for (int i = 0; i < MAX_AGENTS; i++)
			task_farm_.AddTask(new PathfindingTaskAStar(&agents_[i]));

		task_farm_.Run();
	}

	void AgentManager::Update(float& dt)
	{
		// update paths
		if (updating_paths_)
		{
			updating_paths_ = UpdatePaths();
		}

		// update pipeline
		pipeline_.Update();

		// update agent and view
		for (int i = 0; i < MAX_AGENTS; i++)
		{
			agents_[i].Update(dt);
			agent_views_[i].Update();
		}
	}

	void AgentManager::Render()
	{
		for (int i = 0; i < MAX_AGENTS; i++)
		{
			agent_views_[i].Render();
		}
	}

	void AgentManager::RefreshAgentPaths()
	{
		WaitAgents();
		RefreshAgentGoals();
		updating_paths_ = true;
	}

	void AgentManager::WaitAgents()
	{
		for (int i = 0; i < MAX_AGENTS; ++i)
		{
			if (!agents_[i].waiting())
				agents_[i].set_wait_at_next_node(true);
		}
	}

	void AgentManager::RefreshAgentGoals()
	{
		for (int i = 0; i < MAX_AGENTS; ++i)
		{
			// get & set a new goal
			Node& random_goal_node = map_->GetRandomNode();
			agents_[i].SetNewGoal(random_goal_node);
		}
	}

	bool AgentManager::UpdatePaths()
	{
		bool result = false;
		for (int i = 0; i < MAX_AGENTS; ++i)
		{
			if (agents_[i].wait_at_next_node())		// if agent is moving towards wait node
			{
				result = true;						// keep updating
			}
			else if (agents_[i].waiting())			// if an agent is waiting
			{
				if (!agents_[i].got_path())			// if agent does not have a path
				{
					if (!pipeline_.running())			// if pipeline is clear
					{
						pipeline_.Run(&agents_[i]);		// run pipeline
						return true;					// break from loop
					}
					result = true;						// keep updating
				}
			}
		}
		return result;
	}

}