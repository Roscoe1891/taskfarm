#ifndef _AGENT_MANAGER_H
#define _AGENT_MANAGER_H

#include "agent.h"

#include "model\map.h"
#include "View\agent_view.h"

#include "threads\farm.h"
#include "threads\pathfinding_task_astar.h"
#include "threads\init_agent_task.h"
#include "threads\pipeline.h"

namespace AP
{

#define MAX_AGENTS 20

	class AgentManager
	{
	public:
		AgentManager();
		~AgentManager();
		void Init(Map* map);
		void Update(float& dt);
		void Render();

		void RefreshAgentPaths();					// Sets new goals and starts path updating

	private:
		Agent agents_[MAX_AGENTS];					// agents
		AgentView agent_views_[MAX_AGENTS];			// agents' view

		Farm task_farm_;							// Task farm 
		Pipeline pipeline_;							// pipeline

		void WaitAgents();							// Set all agents to wait at the next node they reach
		void RefreshAgentGoals();					// Finds and sets new goal nodes for all agents
		bool UpdatePaths();							// checks agents waiting for updated paths and gets path if required

		bool updating_paths_;						// True if paths are currently being updated

		Map* map_;									// pointer to the main map
	};
}

#endif
