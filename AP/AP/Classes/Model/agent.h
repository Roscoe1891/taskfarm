#ifndef _AGENT_H
#define _AGENT_H

#include "Maths\Vector3.h"
#include "pathfinding\pathfinder_astar.h"

namespace AP
{

	struct AgentInitData
	{
		Map* map;
		Node* start;
		Node* goal;
		float speed;
	};

	class Agent
	{
	public:
		Agent();
		~Agent();
		void Init(Vector3 position, float move_speed, NodeMap& node_map, Node& goal);
		void Init(AgentInitData init_data);
		void Update(float& dt);
		void MoveTo(float& dt, Vector3 destination);
		void ResetNodeMap();

		inline Vector3 position() const { return position_; }
	
		inline bool waiting() const { return waiting_; }
		inline void set_waiting(bool new_value) { waiting_ = new_value; }

		inline bool wait_at_next_node() const { return wait_at_next_node_; }
		inline void set_wait_at_next_node(bool new_value) { wait_at_next_node_ = new_value; }

		inline bool got_path() const { return got_path_; }

		void SetNewGoal(Node& goal);
		void FindPath();

	private:
		void CopyNodeMap(NodeMap& node_map);

		Vector3 position_;
		float move_speed_;

		bool wait_at_next_node_;				// true if agent is going to wait at the next node
		bool waiting_;							// true is agent is waiting at a node
		bool got_path_;							// true if agent has a path to follow at the moment

		PathfinderAStar pathfinder_astar_;
		NodeMap node_map_;
		stack<Node*> path_;

		Node* goal_;
	};
}

#endif