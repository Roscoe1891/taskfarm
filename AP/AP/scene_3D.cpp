#include "scene_3D.h"

namespace AP
{
	bool Scene3D::CreatePixelFormat(HDC hdc)
	{
		PIXELFORMATDESCRIPTOR pfd = { 0 };
		int pixelformat;

		pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);	// Set the size of the structure
		pfd.nVersion = 1;							// Always set this to 1
		// Pass in the appropriate OpenGL flags
		pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
		pfd.dwLayerMask = PFD_MAIN_PLANE;			// standard mask (this is ignored anyway)
		pfd.iPixelType = PFD_TYPE_RGBA;				// RGB and Alpha pixel type
		pfd.cColorBits = COLOUR_DEPTH;				// Here we use our #define for the color bits
		pfd.cDepthBits = COLOUR_DEPTH;				// Ignored for RBA
		pfd.cAccumBits = 0;							// nothing for accumulation
		pfd.cStencilBits = 0;						// nothing for stencil

		//Gets a best match on the pixel format as passed in from device
		if ((pixelformat = ChoosePixelFormat(hdc, &pfd)) == false)
		{
			MessageBox(NULL, "ChoosePixelFormat failed", "Error", MB_OK);
			return false;
		}

		//sets the pixel format if its ok. 
		if (SetPixelFormat(hdc, pixelformat, &pfd) == false)
		{
			MessageBox(NULL, "SetPixelFormat failed", "Error", MB_OK);
			return false;
		}

		return true;
	}

	void Scene3D::ResizeGLWindow(int width, int height)// Initialize The GL Window
	{
		if (height == 0)// Prevent A Divide By Zero error
		{
			height = 1;// Make the Height Equal One
		}

		glViewport(0, 0, width, height);

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();

		//calculate angle of view & aspect ratio
		gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 1, 150.0f);

		glMatrixMode(GL_MODELVIEW);// Select The Modelview Matrix
		glLoadIdentity();// Reset The Modelview Matrix
	}

	void Scene3D::InitializeOpenGL(int width, int height)
	{
		hdc_ = GetDC(*hwnd_);//  sets  global HDC

		if (!CreatePixelFormat(hdc_))//  sets  pixel format
			PostQuitMessage(0);

		hrc_ = wglCreateContext(hdc_);	//  creates  rendering context from  hdc
		wglMakeCurrent(hdc_, hrc_);		//	Use this HRC.

		ResizeGLWindow(width, height);	// Setup the Screen
	}

	void Scene3D::Init(HWND* wnd, Input* in)
	{
		hwnd_ = wnd;
		input_ = in;

		GetClientRect(*hwnd_, &screen_rect_);	//get rect into our handy global rect
		InitializeOpenGL(screen_rect_.right, screen_rect_.bottom); // initialise openGL

		//OpenGL settings
		glShadeModel(GL_SMOOTH);							// Enable Smooth Shading
		//glClearColor(0.39f, 0.58f, 93.0f, 1.0f);			// Cornflour Blue Background
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);				// Black Background
		glClearDepth(1.0f);									// Depth Buffer Setup
		glEnable(GL_DEPTH_TEST);							// Enables Depth Testing
		glDepthFunc(GL_LEQUAL);								// The Type Of Depth Testing To Do
		glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// Really Nice Perspective Calculations
		glEnable(GL_LIGHTING);											// Turn on lighting
		glEnable(GL_COLOR_MATERIAL);									// Turn on materials
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);	// Specify Texture calculations
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		
		// define blend function
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		// calculate centres
		client_centre_ = CalcClientCentre(screen_rect_);
		screen_centre_ = CalcScreenCentre(screen_rect_);

		// Initialise camera and controller
		camera_main_.Init(Vector3(0.f, 0.f, 6.f), Vector3(0.f, 0.f, 0.f), Vector3(0.f, 1.f, 0.f));
		camera_controller_main_.ControllerInit(&camera_main_, input_, &client_centre_,  0x57, 0x53, 0x41, 0x44, VK_UP, VK_DOWN, VK_SPACE);
		
		// Initialise Map
		map_.Init(&rng_, 100);
		map_view_.Init(&map_);

		benchmark_.StartTiming();

		// Initialise Agent Manager
		agent_manager_.Init(&map_);

		benchmark_.StopTiming();
		benchmark_.RecordResult();
		benchmark_.OutputResult();

		// turn on a light
		glEnable(GL_LIGHT0);
		GLfloat ambient_light[4] = { 0.4f, 0.4f, 0.4f, 1.f };
		glLightfv(GL_LIGHT0, GL_AMBIENT, ambient_light);

		// wireframe
		glPolygonMode(GL_BACK, GL_LINE);

		// Centre mouse
		input_->set_mouse_pos(screen_centre_);
		ShowCursor(false);

	}

	void Scene3D::Update(float dt)
	{
		// centre mouse
		input_->set_mouse_pos(screen_centre_);
		
		// refresh agent paths on pressing P
		if (input_->KeyPressed(0x50))
		{
			agent_manager_.RefreshAgentPaths();
		}

		// update main camera controller
		camera_controller_main_.Update(dt);
		
		// update agent manager
		agent_manager_.Update(dt);

		// update input
		input_->Update();
		
		// Render scene once all updating has been complete
		Render();
	}

	void Scene3D::Render()
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);// Clear The Screen And The Depth Buffer
		glLoadIdentity();// load Identity Matrix

		// set glu Lookat in relation to camera
		gluLookAt(
			camera_main_.position().x(), camera_main_.position().y(), camera_main_.position().z(),
			camera_main_.look_at().x(), camera_main_.look_at().y(), camera_main_.look_at().z(),
			camera_main_.up().x(), camera_main_.up().y(), camera_main_.up().z()
			);
		
		glPushMatrix();
			
			// position and draw map
			glTranslatef(-(WIDTH * 0.5f), -(HEIGHT * 0.25), -(HEIGHT * 0.6f));
			glRotatef(-45.0, 1.0f, 0.0f, 0.0f);
			map_view_.Render();
			
			// draw angents
			agent_manager_.Render();

		glPopMatrix();

		SwapBuffers(hdc_);// Swap the frame buffers.
	}

	void Scene3D::Resize()
	{
		if (hwnd_ == NULL)
			return;

		GetClientRect(*hwnd_, &screen_rect_);
		ResizeGLWindow(screen_rect_.right, screen_rect_.bottom);

		client_centre_ = CalcClientCentre(screen_rect_);
		screen_centre_ = CalcScreenCentre(screen_rect_);
	}

	POINT Scene3D::CalcClientCentre(RECT& screen)
	{
		POINT centre;
		centre.x = (long)(screen.right / 2);
		centre.y = (long)(screen.bottom / 2);
		return centre;
	}

	POINT Scene3D::CalcScreenCentre(RECT& screen)
	{
		POINT centre = CalcClientCentre(screen);
		ClientToScreen(*hwnd_, &centre);
		return centre;
	}

}

