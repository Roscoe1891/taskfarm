//3D Framework main

// Includes
#include <windows.h>
#include <stdio.h>

#include "scene_3D.h"

#include "utilities/input.h"
#include "utilities/timer.h"

#include "maths/vector3.h"

	// Globals
	HWND hwnd_;
	AP::Scene3D scene_;
	AP::Timer timer_;
	AP::Input input_;
	bool running_ = true;

	//Prototypes
	LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
	void RegisterMyWindow(HINSTANCE);
	BOOL InitialiseMyWindow(HINSTANCE, int);
	int WINAPI WinMain(HINSTANCE, HINSTANCE, PSTR, int);

	// Defines the window
	void RegisterMyWindow(HINSTANCE hInstance)
	{
		WNDCLASSEX  wcex;

		wcex.cbSize = sizeof(wcex);
		wcex.style = CS_HREDRAW | CS_VREDRAW;
		wcex.lpfnWndProc = WndProc;
		wcex.cbClsExtra = 0;
		wcex.cbWndExtra = 0;
		wcex.hInstance = hInstance;
		wcex.hIcon = 0;
		wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
		wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
		wcex.lpszMenuName = NULL;
		wcex.lpszClassName = "FirstWindowClass";
		wcex.hIconSm = 0;

		RegisterClassEx(&wcex);
	}

	// Attempts to create the window and display it
	BOOL InitialiseMyWindow(HINSTANCE hInstance, int nCmdShow)
	{
		hwnd_ = CreateWindow("FirstWindowClass",
			"Camera Window",
			WS_OVERLAPPEDWINDOW,
			CW_USEDEFAULT,
			CW_USEDEFAULT,
			CW_USEDEFAULT,
			CW_USEDEFAULT,
			NULL,
			NULL,
			hInstance,
			NULL);
		if (!hwnd_)
		{
			return FALSE;
		}

		ShowWindow(hwnd_, nCmdShow);
		UpdateWindow(hwnd_);
		return TRUE;

	}

	// Entry point. The program start here
	int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR szCmdLine, int nCmdShow)
	{
		MSG         msg;

		RegisterMyWindow(hInstance);

		if (!InitialiseMyWindow(hInstance, nCmdShow))
			return FALSE;

		scene_.Init(&hwnd_, &input_);
		timer_.Init();

		while (running_)
		{
			if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				if (msg.message == WM_QUIT)
					break;
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}

			else
			{
				// calculate delta time
				timer_.CalcDeltaTime();
				// Update and render scene
				scene_.Update(timer_.delta_time());
			}
			// quit with escape
			if (input_.KeyPressed(VK_ESCAPE))
			{
				running_ = false;
			}
		}

		return msg.wParam;
	}

	// handles window messages				
	LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
	{
		switch (message)
		{
		case WM_CREATE:
			break;

		case WM_SIZE:
			scene_.Resize();
			break;

		case WM_KEYDOWN:
			input_.set_key_down(wParam);
			break;

		case WM_KEYUP:
			input_.set_key_up(wParam);
			break;

		case WM_MOUSEMOVE:
			input_.set_mouse_x(LOWORD(lParam));
			input_.set_mouse_y(HIWORD(lParam));
			break;

		case WM_DESTROY:
			PostQuitMessage(0);
			break;

		}

		return DefWindowProc(hwnd, message, wParam, lParam);

	}

