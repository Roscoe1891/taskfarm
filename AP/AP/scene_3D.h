#ifndef _SCENE3D_H
#define _SCENE3D_H

#include <windows.h>
#include <stdio.h>
#include <mmsystem.h>
#include <math.h>
#include <gl\gl.h>
#include <gl\glu.h>

#include "utilities\Input.h"
#include "utilities\rng.h"
#include "utilities\benchmark.h"

#include "camera\camera.h"
#include "camera\camera_controller.h"
#include "camera\camera_controller_user.h"

#include "pathfinding\pathfinder_astar.h"
#include "pathfinding\pathfinder_lee.h"

#include "model\agent_manager.h"
#include "model\map.h"
#include "model\node.h"

#include "view\map_view.h"

// Scene 3D
// The main class for the 3D scene to be displayed in the window

namespace AP
{

#define COLOUR_DEPTH 16	//Colour depth
//#define MAX_AGENTS 100

	class Scene3D
	{
	public:
		void Init(HWND* hwnd, Input* in);	// initialise
		void Update(float dt);				// update scene
		void Render();						// render scene
		void Resize();						// resize scene window

	protected:
		bool CreatePixelFormat(HDC hdc);				// Create Pixel Format
		void ResizeGLWindow(int width, int height);		// Resize GL Window to width x height
		void InitializeOpenGL(int width, int height);	// Initialise OpenGL setting based of window of size width x height
		POINT CalcScreenCentre(RECT& screen);			// Calculate and return centre (screen co-ords)
		POINT CalcClientCentre(RECT& screen);			// Calculate and return centre (client co-ords)

		//vars
		HWND* hwnd_;								// handle to the window
		Input* input_;								// pointer to input
		RECT screen_rect_;							// rectangle formed by the window which forms the 'screen'
		POINT screen_centre_;						// the centre of the window in screen co-ords
		POINT client_centre_;						// the centre of the window in client co-ords
		HDC	hdc_;									// handle to device context
		HGLRC hrc_;									// hardware RENDERING CONTEXT
		
		RNG rng_;									// Random Number Generator
		
		Map map_;									// The map
		MapView map_view_;							// The map view

		AgentManager agent_manager_;				// agent manager

		Benchmark benchmark_;						// Benchmark for testing

		// Camera
		Camera camera_main_;							// The main camera 
		CameraControllerUser camera_controller_main_;	// The controller for the main camera
	};

}

#endif